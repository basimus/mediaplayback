package ph.edu.usjr.mediaplayback

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import java.util.ArrayList
import android.widget.Toast
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.util.Util.getUserAgent
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray



class QuizActivity : AppCompatActivity(), View.OnClickListener, ExoPlayer.EventListener
{

    private val mButtonIDs = intArrayOf(R.id.buttonA, R.id.buttonB, R.id.buttonC, R.id.buttonD)
    private var mRemainingSampleIDs: ArrayList<Int>? = null
    private var mQuestionSampleIDs: ArrayList<Int>? = null
    private var mAnswerSampleID: Int = 0
    private var mCurrentScore: Int = 0
    private var mHighScore: Int = 0
    private lateinit var mButtons: Array<Button?>
    private  var mExoPlayer : SimpleExoPlayer? = null
    private lateinit var mPlayerView : SimpleExoPlayerView
    private lateinit var mStateBuilder : PlaybackStateCompat.Builder

    private lateinit var mNotificationManager: NotificationManager
    companion object {
        const val TAG = "TAGG"
        private val CORRECT_ANSWER_DELAY_MILLIS = 1000
        private val REMAINING_SONGS_KEY = "remaining_songs"
        lateinit var mMediaSession: MediaSessionCompat
    }


    override fun onCreate( savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        val isNewGame = !intent.hasExtra(REMAINING_SONGS_KEY)

        mPlayerView = findViewById(R.id.playerView)

        if (isNewGame) {
            QuizUtils.setCurrentScore(this, 0)
            mRemainingSampleIDs = Sample.getAllSampleIDs(this)

        } else {
            mRemainingSampleIDs = intent.getIntegerArrayListExtra(REMAINING_SONGS_KEY)
        }

        mCurrentScore = QuizUtils.getCurrentScore(this)
        mHighScore = QuizUtils.getHighScore(this)


        mQuestionSampleIDs = QuizUtils.generateQuestion(mRemainingSampleIDs!!)
        mAnswerSampleID = QuizUtils.getCorrectAnswerID(mQuestionSampleIDs!!)
        mPlayerView.defaultArtwork = BitmapFactory.decodeResource(resources,R.drawable.question_mark)
        if (mQuestionSampleIDs!!.size < 2) {
            QuizUtils.endGame(this)
            finish()
        }

        mButtons = initializeButtons(mQuestionSampleIDs!!)

        initializeMediaSession()
        val answerSample = Sample.getSampleByID(this, mAnswerSampleID)
        if (answerSample == null) {
            Toast.makeText(this, getString(R.string.sample_not_found_error),
                Toast.LENGTH_SHORT).show()
            return
        }

        initializePlayer(Uri.parse(answerSample.uri))
        showNotification(mStateBuilder.build())

    }

    private fun initializePlayer(mediaUri: Uri) {
        if (mExoPlayer == null) {
            val trackSelector = DefaultTrackSelector()
            val loadControl = DefaultLoadControl()
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl)
            mPlayerView.player = mExoPlayer

            val userAgent = getUserAgent(this, "ClassicalMusicQuiz")

            val mediaSource = ExtractorMediaSource(mediaUri, DefaultDataSourceFactory(
                this, userAgent), DefaultExtractorsFactory(), null, null)
            mExoPlayer!!.prepare(mediaSource)

            mExoPlayer!!.playWhenReady = true
            showNotification(mStateBuilder.build())
        }
    }

    private fun showNotification(state: PlaybackStateCompat) {
        val builder = NotificationCompat.Builder(this)

        val icon: Int
        val play_pause: String

        when(state.state){
            PlaybackStateCompat.STATE_PLAYING ->{
                icon = R.drawable.exo_controls_pause
                play_pause = getString(R.string.pause)
            }
            else ->{
                icon = R.drawable.exo_controls_play
                play_pause = getString(R.string.play)
            }
        }
        val playPauseAction = android.support.v4.app.NotificationCompat.Action(
            icon, play_pause,
            MediaButtonReceiver.buildMediaButtonPendingIntent(this,
                PlaybackStateCompat.ACTION_PLAY_PAUSE))

        val restartAction = android.support.v4.app.NotificationCompat.Action(R.drawable.exo_controls_previous, getString(R.string.restart),
            MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS))

        val contentPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, QuizActivity::class.java), 0)

        builder.setContentTitle(getString(R.string.guess))
            .setContentText(getString(R.string.notification_text))
            .setContentIntent(contentPendingIntent)
            .setSmallIcon(R.drawable.ic_music_note)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .addAction(restartAction)
            .addAction(playPauseAction)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(0, builder.build())
    }

    private fun initializeButtons(answerSampleIDs: ArrayList<Int>): Array<Button?> {
        val buttons = arrayOfNulls<Button>(mButtonIDs.size)

        for (i in answerSampleIDs.indices) {
            val currentButton = findViewById<View>(mButtonIDs[i]) as Button
            val currentSample = Sample.getSampleByID(this, answerSampleIDs[i])
            buttons[i] = currentButton
            currentButton.setOnClickListener(this)
            if (currentSample != null) {
                currentButton.text = currentSample.composer
            }
        }
        return buttons
    }

    override fun onClick(v: View) {

        showCorrectAnswer()

        val pressedButton = v as Button

        var userAnswerIndex = -1
        for (i in mButtons.indices) {
            if (pressedButton.id == mButtonIDs[i]) {
                userAnswerIndex = i
            }
        }

        val userAnswerSampleID = mQuestionSampleIDs!![userAnswerIndex]

        if (QuizUtils.userCorrect(mAnswerSampleID, userAnswerSampleID)) {
            mCurrentScore++
            QuizUtils.setCurrentScore(this, mCurrentScore)
            if (mCurrentScore > mHighScore) {
                mHighScore = mCurrentScore
                QuizUtils.setHighScore(this, mHighScore)
            }
        }

        mRemainingSampleIDs!!.remove(Integer.valueOf(mAnswerSampleID))

        val handler = Handler()
        handler.postDelayed({
            val nextQuestionIntent = Intent(this@QuizActivity, QuizActivity::class.java)
            nextQuestionIntent.putExtra(REMAINING_SONGS_KEY, mRemainingSampleIDs)
            finish()
            startActivity(nextQuestionIntent)
        }, CORRECT_ANSWER_DELAY_MILLIS.toLong())

    }

    private fun showCorrectAnswer() {
        for (i in mQuestionSampleIDs!!.indices) {
            val buttonSampleID = mQuestionSampleIDs!![i]

            mButtons[i]!!.isEnabled = false
            mPlayerView.defaultArtwork = Sample.getComposerArtBySampleID(this,mAnswerSampleID)
            if (buttonSampleID == mAnswerSampleID) {
                mButtons[i]!!.background.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_light),
                    PorterDuff.Mode.MULTIPLY)
                mButtons[i]!!.setTextColor(Color.WHITE)
            } else {
                mButtons[i]!!.background.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_light),
                    PorterDuff.Mode.MULTIPLY)
                mButtons[i]!!.setTextColor(Color.WHITE)

            }
        }
    }


    private fun releasePlayer() {

        mExoPlayer!!.stop()
        mExoPlayer!!.release()
        mExoPlayer = null
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    private fun initializeMediaSession() {

        mMediaSession = MediaSessionCompat(this, TAG)

        mMediaSession.setFlags(
            MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)

        mMediaSession.setMediaButtonReceiver(null)

        mStateBuilder = PlaybackStateCompat.Builder()
            .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1.0f)
            .setActions(
                PlaybackStateCompat.ACTION_PLAY or
                        PlaybackStateCompat.ACTION_PAUSE or
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                        PlaybackStateCompat.ACTION_PLAY_PAUSE)

        mMediaSession.setPlaybackState(mStateBuilder.build())
        showNotification(mStateBuilder.build())
        mMediaSession.setCallback(MySessionCallback())
        mMediaSession.isActive = true
    }


    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if (playbackState == ExoPlayer.STATE_READY && playWhenReady) {
            mStateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                mExoPlayer!!.currentPosition, 1f)
        }else if(playbackState == ExoPlayer.STATE_READY && !playWhenReady){
            mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                mExoPlayer!!.currentPosition, 1f)
        }
        mMediaSession.setPlaybackState(mStateBuilder.build())
        showNotification(mStateBuilder.build())
    }

    override fun onResume() {
        super.onResume()
        initializeMediaSession()
    }


    override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {

    }

    override fun onPlayerError(error: ExoPlaybackException?) {

    }

    override fun onLoadingChanged(isLoading: Boolean) {

    }

    override fun onPositionDiscontinuity() {

    }

    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {

    }

    private inner class MySessionCallback : MediaSessionCompat.Callback() {
        override fun onPlay() {
            mExoPlayer!!.playWhenReady = true
        }

        override fun onPause() {
            mExoPlayer!!.playWhenReady = false
        }

        override fun onSkipToPrevious() {
            mExoPlayer!!.seekTo(0)

        }
    }

}

class MediaReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        MediaButtonReceiver.handleIntent(QuizActivity.mMediaSession,p1)

    }

}

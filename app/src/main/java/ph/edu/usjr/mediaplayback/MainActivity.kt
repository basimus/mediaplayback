package ph.edu.usjr.mediaplayback

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private val GAME_FINISHED = "game_finished"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val highScoreTextView = findViewById<TextView>(R.id.highscoreText)


        val highScore = QuizUtils.getHighScore(this)
        val maxScore = Sample.getAllSampleIDs(this).size - 1

        val highScoreText = getString(R.string.high_score, highScore, maxScore)
        highScoreTextView.text = highScoreText

        if (intent.hasExtra(GAME_FINISHED)) {
            val gameFinishedTextView = findViewById<TextView>(R.id.gameResult)
            val yourScoreTextView = findViewById<TextView>(R.id.resultScore)

            val yourScore = QuizUtils.getCurrentScore(this)
            val yourScoreText = getString(R.string.score_result, yourScore, maxScore)
            yourScoreTextView.text = yourScoreText

            gameFinishedTextView.visibility = View.VISIBLE
            yourScoreTextView.visibility = View.VISIBLE
        }
    }


    fun newGame(view: View) {
        val quizIntent = Intent(this, QuizActivity::class.java)
        startActivity(quizIntent)
    }
}

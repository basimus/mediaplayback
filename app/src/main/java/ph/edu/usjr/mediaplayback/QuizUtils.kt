package ph.edu.usjr.mediaplayback


import android.content.Context
import android.content.Intent
import java.util.*


internal object QuizUtils {

    private val CURRENT_SCORE_KEY = "current_score"
    private val HIGH_SCORE_KEY = "high_score"
    private val GAME_FINISHED = "game_finished"
    private val NUM_ANSWERS = 4

    fun generateQuestion(remainingSampleIDs: ArrayList<Int>): ArrayList<Int> {


        Collections.shuffle(remainingSampleIDs)

        val answers = ArrayList<Int>()

        for (i in 0 until NUM_ANSWERS) {
            if (i < remainingSampleIDs.size) {
                answers.add(remainingSampleIDs[i])
            }
        }

        return answers
    }

    fun getHighScore(context: Context): Int {
        val mPreferences = context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        return mPreferences.getInt(HIGH_SCORE_KEY, 0)
    }

    fun setHighScore(context: Context, highScore: Int) {
        val mPreferences = context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val editor = mPreferences.edit()
        editor.putInt(HIGH_SCORE_KEY, highScore)
        editor.apply()
    }

    fun getCurrentScore(context: Context): Int {
        val mPreferences = context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        return mPreferences.getInt(CURRENT_SCORE_KEY, 0)
    }

    fun setCurrentScore(context: Context, currentScore: Int) {
        val mPreferences = context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val editor = mPreferences.edit()
        editor.putInt(CURRENT_SCORE_KEY, currentScore)
        editor.apply()
    }

    fun getCorrectAnswerID(answers: ArrayList<Int>): Int {
        val r = Random()
        val answerIndex = r.nextInt(answers.size)
        return answers[answerIndex]
    }


    fun userCorrect(correctAnswer: Int, userAnswer: Int): Boolean {
        return userAnswer == correctAnswer
    }

    fun endGame(context: Context) {
        val endGame = Intent(context, MainActivity::class.java)
        endGame.putExtra(GAME_FINISHED, true)
        context.startActivity(endGame)
    }

}
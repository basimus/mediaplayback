package ph.edu.usjr.mediaplayback

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.JsonReader
import android.widget.Toast
import com.google.android.exoplayer2.upstream.DataSourceInputStream
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.google.android.exoplayer2.util.Util
import java.io.IOException
import java.io.InputStreamReader
import java.util.ArrayList


internal class Sample private constructor(var sampleID: Int, var composer: String?,

 var title: String?, var uri: String?, var albumArtID: String?) {
    companion object {

        fun getComposerArtBySampleID(context: Context, sampleID: Int): Bitmap {
            val sample = Sample.getSampleByID(context, sampleID)
            val albumArtID = context.resources.getIdentifier(
                sample?.albumArtID, "drawable",
                context.packageName)
            return BitmapFactory.decodeResource(context.resources, albumArtID)
        }

        fun getSampleByID(context: Context, sampleID: Int): Sample? {
            val reader: JsonReader
            try {
                reader = readJSONFile(context)
                reader.beginArray()
                while (reader.hasNext()) {
                    val currentSample = readEntry(reader)
                    if (currentSample.sampleID == sampleID) {
                        reader.close()
                        return currentSample
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        fun getAllSampleIDs(context: Context): ArrayList<Int> {
            val reader: JsonReader
            val sampleIDs = ArrayList<Int>()
            try {
                reader = readJSONFile(context)
                reader.beginArray()
                while (reader.hasNext()) {
                    sampleIDs.add(readEntry(reader).sampleID)
                }
                reader.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return sampleIDs
        }

        private fun readEntry(reader: JsonReader): Sample {
            var id: Int? = -1
            var composer: String? = null
            var title: String? = null
            var uri: String? = null
            var albumArtID: String? = null

            try {
                reader.beginObject()
                while (reader.hasNext()) {
                    val name = reader.nextName()
                    when (name) {
                        "name" -> title = reader.nextString()
                        "id" -> id = reader.nextInt()
                        "composer" -> composer = reader.nextString()
                        "uri" -> uri = reader.nextString()
                        "albumArtID" -> albumArtID = reader.nextString()
                        else -> {
                        }
                    }
                }
                reader.endObject()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return Sample(id!!, composer, title, uri, albumArtID)
        }

        @Throws(IOException::class)
        private fun readJSONFile(context: Context): JsonReader {
            val assetManager = context.assets
            var uri: String? = null

            try {
                for (asset in assetManager.list("")!!) {
                    if (asset.endsWith(".exolist.json")) {
                        uri = "asset:///$asset"
                    }
                }
            } catch (e: IOException) {
                Toast.makeText(context, R.string.sample_list_load_error, Toast.LENGTH_LONG)
                    .show()
            }

            val userAgent = Util.getUserAgent(context, "ClassicalMusicQuiz")
            val dataSource = DefaultDataSource(context, null, userAgent, false)
            val dataSpec = DataSpec(Uri.parse(uri))
            val inputStream = DataSourceInputStream(dataSource, dataSpec)

            var reader: JsonReader? = null
            try {
                reader = JsonReader(InputStreamReader(inputStream, "UTF-8"))
            } finally {
                Util.closeQuietly(dataSource)
            }
            return reader!!


        }
    }


}